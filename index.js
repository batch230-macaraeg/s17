console.log("Hello World!");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/
		

	
	function printWelcomeMessage(){
		let fullName = prompt("What is your name? " );
		let age = prompt("How old are you? ");
		let address = prompt("Where do you live?")

		console.log("Hello," +fullName+"!");
		console.log("You are " +age+ "years old." );
		console.log("You live in " +address );
	}

	
	printWelcomeMessage();


	function musicalArtist(){
		console.log("1. The Beatles");
		console.log("2. Metallica");
		console.log("3. The Eagles");
		console.log("4. L'arc~en~Ciel");
		console.log("5. Eraserheads");
	}

	musicalArtist();

	// 1ST
	let movieRating1 = function movieRate(){
		console.log("1. The Godfather");
	}

	movieRating1();

	movieRating1 = function(){
		console.log("Rotten Tomatoes Rating: 97%");
	}

	movieRating1();

	// 2ND
	let movieRating2 = function movieRate(){
		console.log("2. The Godfather, Part II");
	}

	movieRating2();

	movieRating2 = function(){
		console.log("Rotten Tomatoes Rating: 96%");
	}

	movieRating2();

	// 3RD
	let movieRating3 = function movieRate(){
		console.log("3. Shawshank Redemption");
	}

	movieRating3();

	movieRating3 = function(){
		console.log("Rotten Tomatoes Rating: 91%");
	}

	movieRating3();

	// 4TH
	let movieRating4 = function movieRate(){
		console.log("4. To Kill Mockingbird");
	}

	movieRating4();

	movieRating4 = function(){
		console.log("Rotten Tomatoes Rating: 93%");
	}

	movieRating4();

	// 5TH
	let movieRating5 = function movieRate(){
		console.log("5. Psycho");
	}

	movieRating5();

	movieRating5 = function(){
		console.log("Rotten Tomatoes Rating: 96%");
	}

	movieRating5();

	// Alert
	function printUsers(){
		alert("Hi! Please add the names of your friends.");
	}

	printUsers();

	function printFriends(){
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with: ");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	}

	
	printFriends();




